Ubuntu-tn Re-Verification
#########################
:date: 2015-09-19 05:22
:author: ubuntiste-msakni
:slug: ubuntu-tn-re-verification

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

Hey!

.. raw:: html

   </p>

Last Tuesday September 15th my LoCo Ubuntu Tunisia had our 4th
ReVerification meeting with the LoCo Council..

.. raw:: html

   </p>

And I'm happy to share with you the news about this by quoting our tweet
about it :)

.. raw:: html

   </p>

    We're so glad to announce that we're
    `#Approved <https://twitter.com/hashtag/Approved?src=hash>`__ for
    the 4th Time in
    ROW!\ `#Congrats <https://twitter.com/hashtag/Congrats?src=hash>`__
    `#LoCo <https://twitter.com/hashtag/LoCo?src=hash>`__, Thanks
    everybody for
    `#contributing <https://twitter.com/hashtag/contributing?src=hash>`__!\ `#Ubuntu <https://twitter.com/hashtag/Ubuntu?src=hash>`__
    `#locteams <https://twitter.com/hashtag/locteams?src=hash>`__

    .. raw:: html

       </p>

    — Ubuntu Tunisia (@UbuntuTn) `September 15,
    2015 <https://twitter.com/UbuntuTn/status/643888038557413377>`__

.. raw:: html

   </p>
   <p>
   <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

I was very happy as the LoCo Council didn't need to ask anything, as
like they said, everything was in the `ReVerification Application
Wiki <https://wiki.ubuntu.com/TunisianTeam/ReVerificationApplication2015>`__

.. raw:: html

   </p>

|image0|

.. raw:: html

   </p>

**Very proud of you my LoCo & I hope that will make better things during
the next two years..**

.. raw:: html

   </p>

If you're Tunisian and reading this, don't forget to join us during the
`SFD <sfd.tn/2015>`__ next month :)

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Ubuntu </ubuntu>`__
-  `Ubuntu-tn </taxonomy/term/16>`__
-  `LoCo </taxonomy/term/17>`__
-  `FOSS </taxonomy/term/7>`__
-  `Linux </taxonomy/term/15>`__
-  `Geek </taxonomy/term/18>`__
-  `ubuntu-planet </taxonomy/term/19>`__
-  `utn </taxonomy/term/20>`__

.. raw:: html

   </div>

.. raw:: html

   </p>

.. |image0| image:: https://wiki.ubuntu.com/TunisianTeam?action=AttachFile&do=get&target=ubuntu-tn.png

