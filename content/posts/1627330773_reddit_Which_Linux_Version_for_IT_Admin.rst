[Reddit] Which Linux Version for IT Admin?
##################################################
:date: Mon 26 Jul 2021 10:00 PM CEST
:author: elacheche
:tags: linux, gnu, admin, noob
:lang: en
:status: published
:slug: reddit_which_linux_version_for_it_admin

This is an old comment I wrote in reddit post_, I find myself using it lately, so I thought it can be a good idea to pubish it here:


	First of all, let start by congratulating you for doing a such move! :-) You'll love the new world you'll explore :-)
	
	I was working as a SysAdmin for the last 6 years, before and during that period, I was contributing to the GNU/Linux community as well by helping people learning and migrating to GNU/Linux world via the Ubuntu community, and here is my feedback..
	
	I'd like to start by explaining briefly the difference between a GNU/Linux distribution and a Linux version.
	
	Basically, Linux is the Kernel, the latest stable version is 4.17, but you do not need only a Kernel to run an OS, you'll need to have the Kernel + Some pre-installed useful programs, that will create a GNU/Linux distribution (aka distro), every distro comes with it's own collection of useful programs, and each have it's own version number and code name, also some are released every X months, others are not, but they just release new versions via a regular update.
	
	The answer to your question will generally result in a list that have 3 categories:
	
	Debian based distros: Debian, Ubuntu(and it's derivatives: Kubuntu, Ubuntu Mate, Xubuntu, Lubuntu) , Devuan(was happy and surprised to see this in the comments), etc
	
	RHEL based distos: RHEL, CentOS, Fedora, etc
	
	Other distros: ArchLinux, Gentoo
	
	Generally talking, if you can use one of those distros, you can easily learn and use the others, differences can be huge, but the idea is the same..
	
	My recommendation is to start by picking a Debian Based distro, and a RHEL based one, then start using it for a period of time, stay away from the GUI, use the CLI to really see the differences, whatever family you like, keep using that family based distro to learn more about it, break things, install things, remove things, don't be afraid of switching between distros..
	
	When you feel ready, bored with your current distro, family go ahead and try ArchLinux or better, try Gentoo, you'll learn too much from the installation experience only, imagine what you can learn if you keep using them :-)
	
	I personally started using GNU/Linux since 2007, my main distro for personal and professional use was Ubuntu, my current Ubuntu partition on personal laptop was first installed on Wed May 11 02:15:40 2011, since then, I only install and uninstall packages to get the distro I like, until I ended up with a minimal system where I removed all useless packages and customized the Desktop Environment/ Window Manager, I crashed that installation multiple times, where I was forced to use CLI only to fix it, and I did, that's how I got new skills.
	
	In the meanwhile, I tried other distros, Debian based and RHEL based, but I always ended up keeping that Ubuntu installation as my main, and I never liked RHEL or ArchLinux..
	
	After 9 years of using Ubuntu as main distro, I switched my personal to Gentoo on Dec 4 2016, because I figure out that I need to learn more about GNU/Linux, and Gentoo is a good place to do so, I still had my Ubuntu partition, but I can't remember when I started it the last time, my optimized/customized Gentoo installation makes it faster for me to use my 10yo laptop (Unless it's compiling Chromium :p ).
	
	My current professional environment is still Debian based, almost all machines are Ubuntu (have a new Devuan box), I also managed to migrate multiple co-workers from Windows to Ubuntu/Ubuntu Mate, some have no IT background..
	
	To summarize, if you feel lost between all the recommendations you got here from the awesome comments, just start by picking a distro and use it, when you feel you need to try a new one go ahead and do :)
	
	Also, don't be afraid to join a community, I was contributing to my local Ubuntu community since 2009 and it's amazing, you learn from people, and you teach others.. Debian, Fedora, CentOS, ArchLinux and Gentoo have awesome communities too.
	
	At the end, don't forget to read about some {Free,Open,Net}BSD too, that's an other world to explore ;-)
	
	What you'll use, and why you'll use it, depends only on you and what you wanna achieve :-)
	
	Good luck in your new journey, and never be afraid of asking about things you don't understand ;-)

.. _post: https://www.reddit.com/r/linux4noobs/comments/8oveqt/which_linux_version_for_it_admin/e06weij/
