The Official Ubuntu Book for Approved LoCos
###########################################
:date: 2014-12-09 09:00
:author: ubuntiste-msakni
:slug: the-official-ubuntu-book-for-approved-locos

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

Hi!

.. raw:: html

   </p>

I know that this is an old story, but I should share it on my blog any
way.. As they say **"Better late than never"**

.. raw:: html

   </p>

I am sharing with you my emails with Ubuntu-tn LoCo Team about The
Official Ubuntu Book.. The conversation can be found in our `LoCo Team
ML
archive <https://lists.ubuntu.com/archives/ubuntu-tn/2014-September/014011.html>`__

.. raw:: html

   </p>

    Hello!

    .. raw:: html

       </p>

    As you know (or maybe not) approved LoCos Teams can ask for the new
    version of `The Official Ubuntu
    Book <http://www.informit.com/store/official-ubuntu-book-9780132748506>`__
    and get a paper version for free.

    .. raw:: html

       </p>

    And for LoCos outside North America can have a digital version of
    the book until the person in charge can figure out a way to ship it
    to the LoCo Team contact.

    .. raw:: html

       </p>

    I already asked to have the book (as we are an approved LoCo Team).

    .. raw:: html

       </p>

    I received **two** (2) codes to download the book, as it is a
    commercial book this means that only two persons can use those codes
    and they **can't** share the book with someone else.

    .. raw:: html

       </p>

    Meanwhile I'm trying to help the person in charge to find a way to
    send us the paper version.

    .. raw:: html

       </p>

    **As the two codes are for personal use only and can't share them
    with everyone I suggest that we use them as gift or prizes during an
    event, SFD maybe!**

    .. raw:: html

       </p>

    We already have the old version of the book (the former LoCo contact
    announced that when he got it), I suggest that we use the old
    version and the new version (if will get it) as prizes too.. What
    are you thinking about that?

.. raw:: html

   </p>

And a
`month <https://lists.ubuntu.com/archives/ubuntu-tn/2014-October/014051.html>`__
later, I got the paper version..

.. raw:: html

   </p>

    Hello @All!

    .. raw:: html

       </p>

    Today I received the paper version of Official Ubuntu Book.

    .. raw:: html

       </p>

    And guess what!?

    .. raw:: html

       </p>

    They sent two copies, not just one :D

    .. raw:: html

       </p>

    On behalf of our LoCo Team I thank you very much Mrs. Heather Fox
    for your help, and thank you Pearson :) :D

.. raw:: html

   </p>
   <center>

|image0|

.. raw:: html

   </center>
   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Ubuntu </ubuntu>`__
-  `Ubuntu-tn </taxonomy/term/16>`__
-  `LoCo </taxonomy/term/17>`__
-  `FOSS </taxonomy/term/7>`__
-  `Linux </taxonomy/term/15>`__
-  `Geek </taxonomy/term/18>`__
-  `ubuntu-planet </taxonomy/term/19>`__

.. raw:: html

   </div>

.. raw:: html

   </p>

.. |image0| image:: https://lists.ubuntu.com/archives/ubuntu-tn/attachments/20141008/cfd70671/attachment.jpe
   :height: 437px
