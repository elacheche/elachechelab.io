A new time slot for the Memebership Board meetings
##################################################
:date: 2015-05-23 20:26
:author: ubuntiste-msakni
:slug: a-new-time-slot-for-the-memebership-board-meetings

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

Earlier this month `Kilos <https://wiki.ubuntu.com/Kilos>`__ (a
Membership Board member) started a discussion on the Membership Board ML
about adding a new slot to the board, the proposition is to have **2
boards on the 1st Thursday of the month, the 1st by 20UTC and the 2nd by
22UTC as usual**..

.. raw:: html

   </p>

After discussing the idea for almost two weeks & after 4 days of the
last email in that thread, I believe that everyone is now OK to add that
3rd slot..

.. raw:: html

   </p>

So I changed the Membership Wiki Page adding the new slot:

.. raw:: html

   </p>

-  https://wiki.ubuntu.com/Membership/Header
-  https://wiki.ubuntu.com/Membership/Boards
-  https://wiki.ubuntu.com/Membership/NewMembert

| If you're want to know more about the Ubuntu Membership please visit
  the **`link <https://wiki.ubuntu.com/Membership>`__**.
| |image0|

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Ubuntu </ubuntu>`__
-  `Ubuntu-tn </taxonomy/term/16>`__
-  `LoCo </taxonomy/term/17>`__
-  `FOSS </taxonomy/term/7>`__
-  `Linux </taxonomy/term/15>`__
-  `Geek </taxonomy/term/18>`__
-  `ubuntu-planet </taxonomy/term/19>`__

.. raw:: html

   </div>

.. raw:: html

   </p>

.. |image0| image:: https://wiki.ubuntu.com/Membership?action=AttachFile&do=get&target=community-rev1.jpg
   :width: 70.0%
