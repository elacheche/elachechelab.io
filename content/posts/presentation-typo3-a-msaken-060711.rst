Présentation Typo3 à M'saken ~ 06/07/11
#######################################
:date: 2011-07-10 10:32
:author: ubuntiste-msakni
:slug: presentation-typo3-a-msaken-060711

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

| Une présentation de Typo3 a était fait Mercredi le 06 Juillet 2011 à
  M'saken, la présentation assuré par notre ami Mehdi Guermazi un
  Tunisien au Quebec, il a pris l’initiative de proposé de faire cette
  présentation juste pour la promotion des logiciels libres..

| La présentation était dans le cadre d'une partenariat entre
  `Ubuntu-TN <http://ubuntu-tn.org>`__ et `L'Association Ibn Sina des
  Sciences et Techniques de M'saken <http://www.aisst.org.tn/>`__.

| Un grand merci pour ceux qui ont était présent, un grand merci à
  l'AISST et U-TN et bien sûr à Mehdi Guermazi.
| 

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Actualités </news>`__
-  `Open Source </foss>`__

.. raw:: html

   </div>

.. raw:: html

   </p>
