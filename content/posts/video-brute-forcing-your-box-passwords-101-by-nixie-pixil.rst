[Video] Brute-forcing your Box : Passwords 101 ~ By Nixie Pixil
###############################################################
:date: 2013-10-18 11:57
:author: ubuntiste-msakni
:slug: video-brute-forcing-your-box-passwords-101-by-nixie-pixil

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

.. raw:: html

   <iframe width="690" height="518" src="//www.youtube.com/embed/cWPhebDXRHg" frameborder="0" allowfullscreen>
   </iframe>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `InfoSec </taxonomy/term/8>`__
-  `Ubuntu </ubuntu>`__

.. raw:: html

   </div>

.. raw:: html

   </p>
