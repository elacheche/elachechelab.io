Membership Board Member Interviews: Svetlana Belkin
###################################################
:date: 2015-09-15 14:21
:author: ubuntiste-msakni
:slug: membership-board-member-interviews-svetlana-belkin

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

The `Ubuntu Membership
Board <https://launchpad.net/%7Eubuntu-membership-board>`__ is
responsible for approving new Ubuntu members. The interview goal is for
the Community to get to know them better and get over the fear of
applying to Membership.

.. raw:: html

   </p>

The interviewee is `Svetlana
Belkin <https://launchpad.net/~belkinsa>`__:

.. raw:: html

   </p>

**What do you do for a career?**

.. raw:: html

   </p>

    Currently I'm working at my local supermarket in the produce
    department but I'm job seeking for a lab tech position within
    biology or even chemistry. But I'm also starting research on how to
    create better non-technical Open \* communities.

    .. raw:: html

       </p>
       <p>

.. raw:: html

   </p>

| 

| What was your first computing experience?
| 

.. raw:: html

   </p>

    My family had a Windows 3.1 almost 20 years ago. I was only four,
    but when I was a bit older, I played computer games on it and even
    was able to break the system without knowing it or how I did it. As
    kids, we are natural hackers.

    .. raw:: html

       </p>
       <p>

.. raw:: html

   </p>

| 

| How long have you been involved with Ubuntu?
| 

.. raw:: html

   </p>

    Since July 2013, but I was using Ubuntu since 2009.

    .. raw:: html

       </p>
       <p>

.. raw:: html

   </p>

| 

| Since you are all fairly new to the Board, why did you join?
| 

.. raw:: html

   </p>

    Wow, I don't even remember why I wanted to join. Perhaps as a
    leadership position or to help others.

    .. raw:: html

       </p>
       <p>

.. raw:: html

   </p>

| 

| What are some of the projects you’ve worked on in Ubuntu over the
  years?
| 

.. raw:: html

   </p>

    I reamped the Ubuntu Women's "Get Involved with Ubuntu!" page, was a
    track lead for UOS may times, and tried to clean up the Community
    Help Wiki.

    .. raw:: html

       </p>
       <p>

.. raw:: html

   </p>

| 

| What is your focus in Ubuntu today?
| 

.. raw:: html

   </p>

    Community building and some Ubuntu (Touch) testing.

    .. raw:: html

       </p>
       <p>

.. raw:: html

   </p>

| 

| Do you contribute to other free/open source projects? Which ones?
| 

.. raw:: html

   </p>

    Just Ubuntu, though I'm involved in the Mozilla Science Lab
    community.

    .. raw:: html

       </p>
       <p>

.. raw:: html

   </p>

| 

| If you were to give a newcomer some advice about getting involved with
  Ubuntu, what would it be?
| 

.. raw:: html

   </p>

    Consider the skills that you have and seek out those teams. Once you
    find them subscribe to their mailing-lists and introduce yourself
    and ask what tasks that they have for you with the skills that you
    have.

    .. raw:: html

       </p>
       <p>

.. raw:: html

   </p>

| 

| Do you have any other comments else you wish to share with the
  community?
| 

.. raw:: html

   </p>

    Don't be afraid to ask questions (even the question of if you have
    enough contributions to apply for Membership).

    .. raw:: html

       </p>
       <p>

.. raw:: html

   </p>
   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Ubuntu </ubuntu>`__
-  `LoCo </taxonomy/term/17>`__
-  `FOSS </taxonomy/term/7>`__
-  `Linux </taxonomy/term/15>`__
-  `ubuntu-planet </taxonomy/term/19>`__

.. raw:: html

   </div>

.. raw:: html

   </p>
