[Tuto]Faire fonctionner Huawei E367(clé 3G++ de TunTel) sous Ubuntu
###################################################################
:date: 2011-09-14 19:19
:author: ubuntiste-msakni
:slug: tutofaire-fonctionner-huawei-e367cle-3g-de-tuntel-sous-ubuntu

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

Bonjour,

.. raw:: html

   </p>

En cherchant une méthode pour faire fonctionner la clè 3G++ de Tunisie
Télécom (c'est une Huawei E367) sur GNU/Linux on trouve plusieurs
méthodes, le plupart entre ces méthodes n'est pas fonctionnel..

.. raw:: html

   </p>

Alors pour être crédible j'ai trouvé la méthode dans le forum
`ZeroShell <http://www.zeroshell.net/fr/>`__, c'est une distribution
Linux pour les serveurs et les systèmes embarqués, un grand merci à
l'utilisateur
`gcams <http://www.zeroshell.net/eng/forum/viewtopic.php?t=2986>`__ qui
à partagé cette solution..

.. raw:: html

   </p>

.. rubric:: Comment faire!!
   :name: comment-faire

.. raw:: html

   </p>

#. Téléchargez ce `script <http://upload.legtux.org/?id=5434659294>`__.
#. Maintenant il faut ouvrir le **Terminal** et rendre le script
   exécutable: chmod +x E367.sh
#. Exécutez le script via le terminal: sudo ./E367.sh
#. Appuyez sur la combinaison **ALT+F2** et tapez:
   **nm-connection-editor**

   .. raw:: html

      </p>

   -  Cliquez sur l'onglet **Connexion mobile à large bande**, puis
      **Ajouter**
   -  Choisissez le nom de votre E367, puis cliquez sur **Suivant**
   -  Choisissez la **Tunisie** comme pays, puis **Suivant**
   -  Sélectionnez **Je ne trouve pas mon fournisseur et je souhaite le
      saisir manuellement** puis saisissez **TUNTEL** comme étant votre
      nom de fournisseur, puis **Suivant**
   -  L'\ **APN** de TT est **internet.tn**, puis **Suivant**
   -  Appuyez sur **Appliquer**
   -  Saisissez votre code **PIN** de carte SIM DATA\ *(PIN par
      défaut:0000)*, puis **Enregistrer**

#. Finalement cliquez sur l'indicateur des réseaux dans le tableau de
   bord et vous trouverez une nouvelle entrée dans liste nommé
   **TUNTEL**

.. raw:: html

   <center>

.. rubric:: Merci infiniment à LuNa
   PeRsa(\ `Ubuntu-TN <http://ubuntu-tn.org>`__ member) la propriétaire
   de la clé..
   :name: merci-infiniment-à-luna-persaubuntu-tn-member-la-propriétaire-de-la-clé..

.. raw:: html

   </p>
   </center>
   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Ubuntu </ubuntu>`__
-  `Open Source </foss>`__

.. raw:: html

   </div>

.. raw:: html

   </p>
