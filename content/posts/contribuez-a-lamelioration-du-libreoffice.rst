Contribuez à l'amélioration du LibreOffice
##########################################
:date: 2011-07-26 23:19
:author: ubuntiste-msakni
:slug: contribuez-a-lamelioration-du-libreoffice

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

| Les développeurs du LibreOffice mènent une enquête pour aider à
  l'évolution du projet, c'est enquête est sous forme d'un QCM..

.. raw:: html

   <center>

`PARTICIPEZ <http://survey.usability-methods.com/survey/943138fd9025419a9a34e7b23e4c6c21/>`__

.. raw:: html

   </center>

LibreOffice 3.3.3 est la version final, cependant la version 3.4.1 est
aussi disponible.

.. raw:: html

   <center>

`Télécharger LibreOffice <http://fr.libreoffice.org/telecharger/>`__

.. raw:: html

   </center>
   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Actualités </news>`__
-  `Ubuntu </ubuntu>`__
-  `Open Source </foss>`__

.. raw:: html

   </div>

.. raw:: html

   </p>
