Software Freedom Day On Tunisie
###############################
:date: 2011-09-20 16:31
:author: Chikori
:slug: software-freedom-day-on-tunisie

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

Le 17 Septembre, il a eu une journée des logiciels libres organisé par
L’IEEE Student branch de l’ENIS et TunAndroid dans un hôtel nommé “Les
oliviers Palace” à Sfax..

.. raw:: html

   </p>
   <center>

|image0|

.. raw:: html

   </center>
   </p>

C’était la **SFD** (**Software Freedom Day**), c’est une célébration
mondiale du Logiciel Libre et Open Source (FOSS). l’objectif dans cette
célébration est d’éduquer le public dans le monde entier au sujet des
avantages de l’utilisation de logiciels libres de haute qualité dans
l’éducation, au gouvernement, à la maison, et dans les entreprises, en
bref,partout!

.. raw:: html

   </p>

L’organisation sans but lucratif Software Freedom International
coordonne SFD au niveau mondial, fournissant un soutien, des cadeaux et
un point de la collaboration, mais les équipes de bénévoles du monde
entier organisent des événements locaux SFD à l’impact de leurs propres
communautés.

.. raw:: html

   </p>

Plusieurs communautés ont participé à cette évent comme Ubuntu-Tn,
Securinets et Mozilla Tunisie.Il a eu aussi une conférence de Monsieur
Chris Dibona de Google USA.

.. raw:: html

   </p>

**`Source photo
SFD <http://www.tunandroid.com/content/software-freedom-day-2011/>`__**

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Open Source </foss>`__
-  `Actualités </news>`__

.. raw:: html

   </div>

.. raw:: html

   </p>

.. |image0| image:: http://www.tunandroid.com/content/wp-content/uploads/2011/09/logo_SFD2011.png

