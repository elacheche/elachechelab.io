Linus Torvalds annonce la sortie de la 3éme version du noyau Linux..
####################################################################
:date: 2011-07-22 10:34
:author: ubuntiste-msakni
:slug: linus-torvalds-annonce-la-sortie-de-la-3eme-version-du-noyau-linux

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

Après sept versions RC, Linus Torvalds, a annoncé aujourd'hui matin la
sortie de la version stable du nouveau noyau Linux.. Cette nouvelle a
était annoncé via `son profile
G+ <https://plus.google.com/102150693225130002912>`__..

.. raw:: html

   </p>

Les trois mots que Linus a écrit on était partagé plus que 1000 fois sur
G+, avec plus de 4500 "+1", sans oubliant de mentionné que le nombre des
commentaires a atteint les limites..

.. raw:: html

   </p>

La nouvelle version est maintenant disponible en téléchargement via le
site officiel `The Linux Kernel Archives <http://kernel.org/>`__

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Actualités </news>`__
-  `Open Source </foss>`__

.. raw:: html

   </div>

.. raw:: html

   </p>
