Ubuntu-Tn LoCo Pack
###################
:date: 2014-12-08 14:57
:author: ubuntiste-msakni
:slug: ubuntu-tn-loco-pack

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

Hi,

.. raw:: html

   </p>

During the last years we had problems getting our LoCo Pack.. As far as
I remember the LoCo Pack is used to be a mixture of Ubuntu Desktop,
Server and Kubuntu CDs, and sometimes with some perks from Canonical..

.. raw:: html

   </p>

And to not break the cycle, we had a problem getting the Ubuntu 14.04
LTS DVD Pack.. We was trying to track our DVDs Pack since May 2014,
contacting Canonical to help us tracking it.. After almost six months
looking for the DVDs Pack I give up.. The canonical's employee (
Michelle ) was very kind and offer us a new one :) :D

.. raw:: html

   </p>

Lately I said "DVD Pack" because it seems like there is two kind of
Packs:

.. raw:: html

   </p>

-  **DVDs Pack** contains the Ubuntu DVDs
-  **LoCo Pack** contains perks from Canonical

So when asking for the new Pack I was asked to choose between DVDs Pack
or LoCo Pack.. I thought that the LoCo Pack will have a mixture between
DVDs and perks.. But I was wrong..

.. raw:: html

   </p>

Last Monday I received the LoCo Pack.. I was so happy to finally get
some extra perks to use during the events.

.. raw:: html

   </p>
   <center>

|image0|

.. raw:: html

   </center>
   </p>

Unfortunately no DVDs inside, my fault.. I'll pay attention next time
what to order.. But I promise my LoCo to find out an easy alternative
during the next months..

.. raw:: html

   </p>

To be more organized I created a Wiki page listing all our `LoCo Team
Inventory <https://wiki.ubuntu.com/TunisianTeam/Events/Inventory>`__
**(Update in progress)**..

.. raw:: html

   </p>

Many thanks for canonical for the support and a special thanks for
**Michel** for her assistance and help during all this period.

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Ubuntu </ubuntu>`__
-  `Ubuntu-tn </taxonomy/term/16>`__
-  `LoCo </taxonomy/term/17>`__
-  `FOSS </taxonomy/term/7>`__
-  `Linux </taxonomy/term/15>`__
-  `Geek </taxonomy/term/18>`__
-  `ubuntu-planet </taxonomy/term/19>`__

.. raw:: html

   </div>

.. raw:: html

   </p>

.. |image0| image:: http://ubuntiste-msakni.legtux.org/images/locopack.jpg
   :height: 600px
