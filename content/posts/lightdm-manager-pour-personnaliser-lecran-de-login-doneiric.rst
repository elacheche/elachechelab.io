LightDM Manager pour personnaliser l’écran de login d’Oneiric
#############################################################
:date: 2011-10-05 09:13
:author: ubuntiste-msakni
:slug: lightdm-manager-pour-personnaliser-lecran-de-login-doneiric

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

**LightDM un gestionnaire de connexion plus léger que GDM, c'est
pourquoi l'équipe de développement de Ubuntu ont choisi de l'utiliser à
la place du GDM. LightDM Manager est le premier outil permettant de
personnaliser LDM.**

.. raw:: html

   </p>

.. raw:: html

   </p>
   <center>

|image0|

.. raw:: html

   </center>
   </p>

LightDM-manager est assez simple, il assure deux type de modifications:

.. raw:: html

   </p>

#. Le changement du fond d’écran
#. Le changement du petit logo visible sur le haut de la zone permettant
   de saisir son mot de passe

.. raw:: html

   <center>

|image1|

.. raw:: html

   </center>
   </p>

.. rubric:: Installation LightDM Manager sur Ubuntu
   :name: installation-lightdm-manager-sur-ubuntu

.. raw:: html

   </p>

``sudo apt-add-repository ppa:claudiocn/slm``

| sudo apt-get update

sudo apt-get install simple-lightdm-manager

.. raw:: html

   </p>
   </p>

**Source Images:**\ `Le
Libiriste <http://www.le-libriste.fr/2011/10/lightdm-manager-pour-customiser-lecran-de-login-dubuntu-11-10-oneiric/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+webdevonlinux%2FHrzu+%28Le+Libriste%29>`__

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Ubuntu </ubuntu>`__
-  `Open Source </foss>`__

.. raw:: html

   </div>

.. raw:: html

   </p>

.. |image0| image:: http://www.le-libriste.fr/wp-content/uploads/2011/10/lightdm_manager-560x313.jpg
.. |image1| image:: http://www.le-libriste.fr/wp-content/uploads/2011/10/Simple_lightDM_Manager_interfaceM.jpg

