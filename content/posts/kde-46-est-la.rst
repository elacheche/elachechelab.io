KDE 4.6 est là!!
################
:date: 2011-01-31 14:24
:author: ubuntiste-msakni
:slug: kde-46-est-la

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

Dérniérement la communauté KDE a annocée la sortie de la dernierre
version du son environnement de travail pour les systèmes GNU/Linux...

.. raw:: html

   <center>

|image0|

.. raw:: html

   <center>

.. raw:: html

   </p>
   <p>

Cette nouvelle version équipait de nouvelle fonctionnalité, pour
télécharger cette dérnierre version vous trouverer toutes les
instruction\ `ICI <http://www.kde.org/info/4.6.0.php>`__.

.. raw:: html

   <center>

|image1|

.. raw:: html

   </center>
   </p>
   <p>
   </center>
   </center>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Actualités </news>`__
-  `Ubuntu </ubuntu>`__
-  `Open Source </foss>`__

.. raw:: html

   </div>

.. raw:: html

   </p>

.. |image0| image:: http://www.kde.org/announcements/4.6/screenshots/thumbs/46-w09.png
.. |image1| image:: http://www.kde.org/images/teaser/460.png

