Full Circle Magazine N° 51
##########################
:date: 2011-07-29 21:51
:author: ubuntiste-msakni
:slug: full-circle-magazine-ndeg-51

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

| Le 51éme numéro du fameuse magazine FCM est sorti.

| Pour c'eux qui connaissent pas FCM, Full Circle est un magazine
  mensuel libre et indépendante dédié à la famille Ubuntu du systèmes
  d'exploitation Linux. Chaque mois, il contient des utiles articles et
  d'histoires des lecteurs. Full Circle dispose également d'un podcast
  compagnon, le podcast Full Circle, qui couvre le magazine avec
  d'autres nouvelles d'intérêt.

.. raw:: html

   <center>

|image0|

.. raw:: html

   </center>

**Ce mois vous trouvez dans FCM:**

.. raw:: html

   </p>

-  Command and Conquer.
-  How-To : Program in Python – Part 25, LibreOffice – Part 6, Ubuntu
   Development – Part 3, and Using KDE (4.6) Part 2.
-  Linux Lab – Creating Your Own Repository.
-  Review – GRAMPS.
-  Top 5 – VOIP Clients.
-  I Think – What Distro(s) Do You Use?
-  Plus: Ubuntu Games, My Story, and much much more!

FCM est disponible en téléchargement via le lien suivant: **`Télécharger
FCM N° 51 <http://fullcirclemagazine.org/issue-51/>`__**

.. raw:: html

   </p>

**Image via `FCM <fullcirclemagazine.org>`__**

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Open Source </foss>`__
-  `Ubuntu </ubuntu>`__

.. raw:: html

   </div>

.. raw:: html

   </p>

.. |image0| image:: http://dl.fullcirclemagazine.org/cover/51/en.jpg

