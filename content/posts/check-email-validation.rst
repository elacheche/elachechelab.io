Check Email Validation
######################
:date: 2014-06-15 13:03
:author: ubuntiste-msakni
:slug: check-email-validation

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

Hey!

.. raw:: html

   </p>

Today am sharing with you a Python script that can helps you verify if
an email address is valid or not.. The script reads the email address
from a text file, so you can make the script check as many emails as
your file contain..

.. raw:: html

   </p>

For now the text file need to have an address per line, I'll try to
change that soon..

.. raw:: html

   </p>

Here is the script, you can find how to use it in the commented lines..

.. raw:: html

   </p>
   <p>
   <script src="https://gist.github.com/elacheche/2eef8f312dd29c8d098e.js"></script>

As it's mentionned in the commants, the script was forked from an other
script created by `Neal
Shyam <https://gist.github.com/nealrs/6722656%20>`__ take a look at it,
maybe you'll find that his script is more useful to you than mine :)

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `FOSS </taxonomy/term/7>`__
-  `Python </taxonomy/term/9>`__
-  `Script </taxonomy/term/10>`__
-  `GitHub </taxonomy/term/11>`__

.. raw:: html

   </div>

.. raw:: html

   </p>
