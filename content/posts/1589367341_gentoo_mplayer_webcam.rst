Test webcam using MPLAYER in Gentoo GNU/Linux
##################################################
:date: Wed 13 May 2020 12:57:28 PM CEST
:author: elacheche
:tags: gentoo, webcam, mplayer
:lang: en
:status: published
:slug: webcam-mplayer

To be able be to use your webcam via mplayer on Gentoo you need to compile it with the `v4l` USE flag, for that you need to add the USE flag to the package.use:

.. code-block:: bash

   # webcam mplayer
   >=media-video/mplayer-1.3.0-r6 v4l


If `mplayer` is already installed you need to re-emerge it using with that new flag:

.. code-block:: bash

   sudo emerge --changed-use -a media-video/mplayer

Now you can test your webcam:

.. code-block:: bash

   mplayer tv:// -tv driver=v4l2:width=640:height=480:device=/dev/video0

