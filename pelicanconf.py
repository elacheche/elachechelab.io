#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'elacheche'
SITENAME = 'Mirror of old blog and drafts'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('Twitter', 'https://twitter.com/elacheche'),
          ('Mastodon', 'https://mastodon.social/@elacheche'),
          ('LinkedIn', 'https://www.linkedin.com/in/elacheche/'),
          ('GitLab', 'https://gitlab.com/elacheche'),
          ('GitHub', 'https://github.com/elacheche'),
          ('', '<a rel="me" href="https://mastodon.tn/@elacheche">mastodon.tn</a>')
        )


DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
