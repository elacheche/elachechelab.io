Project Fedena
##############
:date: 2011-09-14 00:00
:author: ubuntiste-msakni
:slug: project-fedena

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

.. raw:: html

   </p>

.. rubric:: Fedena, c'est quoi?
   :name: fedena-cest-quoi

.. raw:: html

   </p>

-  Fedena est un système open source de gestion scolaire basé sur Ruby
   on Rails. Il a été initialement développé par une équipe de
   développeurs de technologies Foradian. Le projet a été rendu open
   source par Foradian, et est actuellement maintenu par la communauté
   open source.
-  Fedena n'est pas seulement un système infromation étudiant. Il est
   beaucoup plus que cela. Avec ses finances intégrées et module des
   ressources humaines qu'il peut prendre soin de tous les systèmes et
   processus nécessaires pour faire fonctionner efficacement votre
   organisation.
-  Fedena signifie la chose la plus cool en vie, ce qui signifie
   habituellement que vous venez de la royauté ou de la richesse.
   Quelque chose qui est fedena est cool, chaud et rare. Le fedena nom
   est également motivé par Athéna, la déesse grecque de la sagesse.
-  La déesse Athéna apparaît assisté par un hibou. Ainsi, l'équipe
   créative de hibou fedena choisi comme le symbole et le logo de
   dérivés de la face d'un hibou. Hibou est aussi le symbole de la
   sagesse et les connaissances dans de nombreuses mythologies et des
   croyances.

.. raw:: html

   <center>

|image0|

.. raw:: html

   </center>
   </p>

.. rubric:: Comment puis-je voir comment Fedena fonctionne?
   :name: comment-puis-je-voir-comment-fedena-fonctionne

.. raw:: html

   </p>

Un site de démonstration pour Fedena a été mis en place à
`demo.projectfedena.org <http://demo.projectfedena.org>`__. Vous pouvez
vous connecter avec les noms d'utilisateurs et mots de passe suivants:

.. raw:: html

   </p>

-  Comme admin - username - admin, mot de passe - admin123
-  En tant qu'étudiant, - username - 1, mot de passe - 1123
-  Comme les employés - username - E1, mot de passe - E1123

.. raw:: html

   <center>

|image1|

.. raw:: html

   </center>
   </p>

.. rubric:: Pour savoir plus à propos de Fedena, vous pouvez visiter les
   liens ci-dessous:
   :name: pour-savoir-plus-à-propos-de-fedena-vous-pouvez-visiter-les-liens-ci-dessous

.. raw:: html

   </p>

-  `Site web officiel Project Fedena <http://www.projectfedena.org/>`__
-  `Fedena Pro Service Partner <http://www.fedena.com/>`__
-  `Manuel d'utilisation
   gratuit <http://en.wikibooks.org/wiki/Fedena>`__

| 
| **Source images et textes:**

.. raw:: html

   </p>

-  `Site web officiel Project Fedena <http://www.projectfedena.org/>`__
-  `Fedena Pro Service Partner <http://www.fedena.com/>`__
-  `Wikipedia <http://en.wikipedia.org/wiki/Fedena/>`__

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Ubuntu </ubuntu>`__
-  `Open Source </foss>`__

.. raw:: html

   </div>

.. raw:: html

   </p>

.. |image0| image:: http://upload.wikimedia.org/wikipedia/en/1/12/Fedena_SIS_logo.png
.. |image1| image:: http://upload.wikimedia.org/wikipedia/en/8/89/Fedena_dashboard.png
   :width: 50.0%
