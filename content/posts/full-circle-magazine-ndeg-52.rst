Full Circle Magazine N° 52
##########################
:date: 2011-08-27 10:11
:author: ubuntiste-msakni
:slug: full-circle-magazine-ndeg-52

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

| Le 52éme numéro du fameuse magazine FCM est sorti.

.. raw:: html

   <center>

|image0|

.. raw:: html

   </center>

**Ce mois vous trouvez dans FCM:**

.. raw:: html

   </p>

-  Command and Conquer.
-  How-To : Program in Python – Part 26, LibreOffice – Part 7, Ubuntu
   Development – Part 4, GRAMPS – Part 1, and Ubuntu For Business &
   Education.
-  Linux Lab – ZoneMinder CCTV – Part 1
-  Review – ChromBook.
-  I Think – Would you like to see an audio editing series with
   Audacity?
-  Plus: Ubuntu Games, My Story, and much much more!

FCM est disponible en téléchargement via le lien suivant: **`Télécharger
FCM N° 52 <http://fullcirclemagazine.org/issue-52/>`__**

.. raw:: html

   </p>

**Image via `FCM <fullcirclemagazine.org>`__**

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Ubuntu </ubuntu>`__
-  `Open Source </foss>`__

.. raw:: html

   </div>

.. raw:: html

   </p>

.. |image0| image:: http://dl.fullcirclemagazine.org/cover/52/en.jpg

