Google célèbre l’anniversaire de IBN KHALDOUN
#############################################
:date: 2011-05-26 23:51
:author: ubuntiste-msakni
:slug: google-celebre-lanniversaire-de-ibn-khaldoun

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

Comme d'habitude Google ne rate pas l’occasion de fêter l’anniversaire
d'une évènement ou une célébrité.. Cette fois ci
`www.google.tn <http://www.google.tn>`__ célèbre l’anniversaire du
célèbre historien, philosophe, diplomate et homme politique arabe IBN
KHALDOUN.

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Actualités </news>`__

.. raw:: html

   </div>

.. raw:: html

   </p>
