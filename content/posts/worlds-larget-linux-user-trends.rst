World's Larget Linux User Trends
################################
:date: 2014-12-03 15:46
:author: ubuntiste-msakni
:slug: worlds-larget-linux-user-trends

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

|image0|

.. raw:: html

   </p>

.. raw:: html

   <div align="right">

`**Source** <http://www.linuxfoundation.org/news-media/announcements/2014/12/linux-foundation-releases-report-detailing-linux-user-trends-among>`__

.. raw:: html

   </div>

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `FOSS </taxonomy/term/7>`__
-  `Linux </taxonomy/term/15>`__
-  `IT </taxonomy/term/12>`__

.. raw:: html

   </div>

.. raw:: html

   </p>

.. |image0| image:: http://www.linuxfoundation.org/sites/main/files/imagecache/infographic-large/infographics/lf_infogfx_linuxusertrends14.png
   :target: http://www.linuxfoundation.org/publications/linux-foundation/linux-end-user-trends-report-2014
