Google est maintenant Tunisien
##############################
:date: 2011-02-18 21:04
:author: ubuntiste-msakni
:slug: google-est-maintenant-tunisien

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

| Après une longue attente, Google vient d'activer depuis hier son
  domaine en ".tn" à partir de ses data centers.
| `www.google.tn <http://www.google.tn>`__ qui a été enregistré le 14
  mai 2009, dans les bureaux de 3S Global Net, ouvrait sur une page non
  personnalisée aux attentes des Tunisiens. On avait l'impression d'être
  sur une autre page de google.com

| Mais depuis, le 27 janvier, lorsque l'Agence Tunisienne d'Internet a
  supprimé les restrictions autour du .tn, google est devenu libre de
  ses mouvements et a pu héberger le google.tn sous ses serveurs afin de
  le personnaliser et d'y ajouter le mot Tunisie sous son logo.

| En attendant la personnalisation de ses différents services pour la
  Tunisie, Google.tn a mis en avant la recherche en temps réel; un
  service qui rafraîchit les résultats en même temps que la requête est
  tapée dans le cadre de recherche.

Pour certains c'est un rêve qui devient une réalité, alors pour quand
les locaux de Google en Tunisie ?

.. raw:: html

   </p>

**Source:webdo.tn**

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Actualités </news>`__

.. raw:: html

   </div>

.. raw:: html

   </p>
