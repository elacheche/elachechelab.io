Virtual Richard M. Stallman !?!
###############################
:date: 2014-02-03 13:24
:author: ubuntiste-msakni
:slug: virtual-richard-m-stallman

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

Hey again,

.. raw:: html

   </p>

Did you asked yourself if one day **RMS** takes a look at applications
installed on your PC what will he said about how much FOSS apps do you
use?

.. raw:: html

   </p>

Do ou think that RMS will be happy or be annoyed? ;-)

.. raw:: html

   </p>

So if you're a Ubuntu or Debian user you can install the **vrms**
application.. As mentioned in the man page: *"This program analyzes the
currently-installed package list on a Debian GNU/Linux system, and
reports the non-free and contrib packages that are currently installed
to stdout."*

.. raw:: html

   </p>

| To install it you need just to execute
| **# apt-get install vrms**

.. raw:: html

   </p>

| Then execute it:
| **$ vrms**

.. raw:: html

   </p>

| The result for my Laptop is:
| **Non-free packages installed on laptop**

.. raw:: html

   </p>

| adobe-flash-properties-gtk GTK+ control panel for Adobe Flash Player
  plugin versi

| adobe-flash-properties-kde KDE control panel Adobe Flash Player plugin
  version 11

| adobe-flashplugin Adobe Flash Player plugin version 11

| btlive BitTorrent Live Client

| dynamips Cisco 7200/3600/3725/3745/2600/1700 Router Emulator

| libcg Nvidia Cg core runtime library

| oracle-java7-installer Oracle Java(TM) Development Kit (JDK) 7

| skype Wherever you are, wherever they are

| sun-java6-bin Sun Java(TM) Runtime Environment (JRE) 6 (architecture

| sun-java6-jre Sun Java(TM) Runtime Environment (JRE) 6 (architecture

| sun-java6-plugin Java(TM) Plug-in, Java SE 6

| ttf-xfree86-nonfree non-free TrueType fonts from XFree86

| ttytter console Twitter client

| unrar Unarchiver for .rar files (non-free version) - binary

zekr Quranic Study Tool

.. raw:: html

   </p>

Non-free packages with status other than installed on laptop

.. raw:: html

   </p>

| ia32-limbo ( dei) Play LIMBO

| libcggl ( dei) Nvidia Cg Opengl runtime library

| libcuda1-331 ( dei) NVIDIA CUDA runtime library

| skype-bin ( ins) client for Skype VOIP and instant messaging se

teamviewer ( dei) TeamViewer (Remote Control Application)

.. raw:: html

   </p>

Contrib packages installed on laptop

.. raw:: html

   </p>

| conky-all highly configurable system monitor (all features enabl

| gns3 graphical network simulator

| playonlinux front-end for Wine

| ttf-mscorefonts-installer Installer for Microsoft TrueType core fonts

| virtualbox x86 virtualization solution - base binaries

| virtualbox-dkms x86 virtualization solution - kernel module sources fo

virtualbox-qt x86 virtualization solution - Qt based user interface

.. raw:: html

   </p>

Contrib packages with status other than installed on laptop

.. raw:: html

   </p>

virtualbox-4.1 ( dei) Oracle VM VirtualBox

.. raw:: html

   </p>

| 20 non-free packages, 0.5% of 4279 installed packages.

8 contrib packages, 0.2% of 4279 installed packages.

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `Ubuntu </ubuntu>`__
-  `FOSS </taxonomy/term/7>`__
-  `Open Source </foss>`__

.. raw:: html

   </div>

.. raw:: html

   </p>
