IT Career
#########
:date: 2014-06-16 15:53
:author: ubuntiste-msakni
:slug: it-career

.. raw:: html

   <div
   class="field field-name-body field-type-text-with-summary field-label-hidden">

.. raw:: html

   <div class="field-items">

.. raw:: html

   <div class="field-item even" property="content:encoded">

|image0|

IT Training Infographic - Pick Your Path to IT Career Success - An
infographic by the team at `CBT Nuggets <www.cbtnuggets.com>`__

.. raw:: html

   </p>
   <p>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div
   class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above clearfix">

.. rubric:: Tags:
   :name: tags
   :class: field-label

-  `IT </taxonomy/term/12>`__
-  `SysAdmin </SysAdmin>`__
-  `DBA </taxonomy/term/13>`__
-  `DevOps </taxonomy/term/14>`__

.. raw:: html

   </div>

.. raw:: html

   </p>

.. |image0| image:: http://www.cbtnuggets.com/static/images/infographic/CBTNuggets-CareerPathInfographicFinal.jpg?v=1.1
   :width: 540px
